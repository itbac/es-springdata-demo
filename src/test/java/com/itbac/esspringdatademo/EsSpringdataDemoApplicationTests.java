package com.itbac.esspringdatademo;

import com.itbac.esspringdatademo.bean.Product;
import com.itbac.esspringdatademo.dao.ProductDao;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.TermQueryBuilder;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest
class EsSpringdataDemoApplicationTests {

    @Autowired
    private ElasticsearchRestTemplate elasticsearchRestTemplate;

    @Autowired
    private ProductDao productDao;

    @Test
    public void createIndex() {
        System.out.println("自动创建索引");
    }

    @Test
    public void deleteIndex() {
        boolean b = elasticsearchRestTemplate.deleteIndex(Product.class);
        System.out.println("删除索引结果：" + b);
    }

    @Test
    public void save() {
        Product product = new Product();
        product.setId(1L);
        product.setTitle("华为手机");
        product.setCategory("手机");
        product.setPrice(2999.0);
        product.setImages("https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fnimg.ws.126.net%2F%3Furl%3Dhttp%253A%252F%252Fdingyue.ws.126.net%252F2021%252F0616%252F6bca47ccj00qur6b1001dc000hs00vkc.jpg%26thumbnail%3D650x2147483647%26quality%3D80%26type%3Djpg&refer=http%3A%2F%2Fnimg.ws.126.net&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1636200519&t=b10f9736e850a2fb6c5c9335490804c2");
        productDao.save(product);
    }

    @Test
    public void update() {
        Product product = new Product();
        product.setId(1L);
        product.setTitle("小米手机");
        product.setCategory("手机");
        product.setPrice(1688.0);
        product.setImages("https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fnimg.ws.126.net%2F%3Furl%3Dhttp%253A%252F%252Fdingyue.ws.126.net%252F2021%252F0616%252F6bca47ccj00qur6b1001dc000hs00vkc.jpg%26thumbnail%3D650x2147483647%26quality%3D80%26type%3Djpg&refer=http%3A%2F%2Fnimg.ws.126.net&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1636200519&t=b10f9736e850a2fb6c5c9335490804c2");
        //更新也是用save
        productDao.save(product);
    }

    @Test
    public void findById() {
        Optional<Product> optional = productDao.findById(1L);

        optional.ifPresent(System.out::println);

    }

    @Test
    public void findAll() {
        Iterable<Product> all = productDao.findAll();
        all.forEach(product -> {
            System.out.println("findAll结果：=>>>>> " + product);
        });
    }

    @Test
    public void delete() {
        Product product = new Product();
        product.setId(1L);
        //根据对象删除
        productDao.delete(product);
        //根据主键删除
        productDao.deleteById(2L);
    }

    @Test
    public void saveAll() {
        List<Product> list = new ArrayList<>();
        for (int i = 1; i <= 10; i++) {
            Product product = new Product();
            product.setId((long) i);
            product.setTitle("小米手机" + i);
            product.setCategory("手机");
            product.setPrice(1688.0 + i);
            product.setImages("https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fnimg.ws.126.net%2F%3Furl%3Dhttp%253A%252F%252Fdingyue.ws.126.net%252F2021%252F0616%252F6bca47ccj00qur6b1001dc000hs00vkc.jpg%26thumbnail%3D650x2147483647%26quality%3D80%26type%3Djpg&refer=http%3A%2F%2Fnimg.ws.126.net&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1636200519&t=b10f9736e850a2fb6c5c9335490804c2");
            list.add(product);
        }
        productDao.saveAll(list);
    }

    //分页查询
    @Test
    public void findByPageable() {
        Sort sort = Sort.by(Sort.Direction.DESC, "id");
        //page当前页 ,第一页从0开始。第二页是1.
        int page = 0;
        int size = 5;
        //分页请求，并排序
        PageRequest pageRequest = PageRequest.of(page, size, sort);
        Page<Product> productPage = productDao.findAll(pageRequest);
        for (Product product : productPage.getContent()) {
            System.out.println("findByPageable 结果 =>>>>>");
        }
    }
    /**
     * 条件查询,是否分页？需要试验。
     */
    @Test
    public void termQuery() {
        Sort sort = Sort.by(Sort.Direction.DESC, "id");
        //page当前页 ,第一页从0开始。第二页是1.
        int page = 0;
        int size = 5;
        //分页请求，并排序
        PageRequest pageRequest = PageRequest.of(page, size, sort);

        TermQueryBuilder termQueryBuilder = QueryBuilders.termQuery("title", "小米");
        Iterable<Product> products = productDao.search(termQueryBuilder,pageRequest);
        if (products.iterator().hasNext()) {
            System.out.println("termQuery 结果： =>>>>>>" + products.iterator().next());
        }
    }
    /**
     * 条件查询，加分页
     */
    @Test
    public void termQuery2() {
        TermQueryBuilder termQueryBuilder = QueryBuilders.termQuery("title", "小米");
        Iterable<Product> products = productDao.search(termQueryBuilder);
        if (products.iterator().hasNext()) {
            System.out.println("termQuery 结果： =>>>>>>" + products.iterator().next());
        }
    }


}
