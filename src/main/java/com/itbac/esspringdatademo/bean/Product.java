package com.itbac.esspringdatademo.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

/**
 * 商品实体类
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
//indexName 索引，shards 分片， replicas 副本,createIndex 创建索引
@Document(indexName = "product", shards = 1, replicas = 1, createIndex = true)
public class Product {
    //商品唯一标识
    @Id
    private Long id;
    //商品名称
    // Text : 分词 ,  analyzer = "ik_max_word" 分词器
    @Field(type = FieldType.Text)
    private String title;
    //分类名称
    //Keyword 关键词，不分词。
    @Field(type = FieldType.Keyword)
    private String category;
    //商品价格
    //Double数字
    @Field(type = FieldType.Double)
    private Double price;
    //图片地址
    //Keyword 不分词,index = false 不索引,不用于查询搜索。
    @Field(type = FieldType.Keyword, index = false)
    private String images;

}
