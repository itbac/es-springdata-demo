package com.itbac.esspringdatademo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EsSpringdataDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(EsSpringdataDemoApplication.class, args);
	}

}
