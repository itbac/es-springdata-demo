package com.itbac.esspringdatademo.dao;

import com.itbac.esspringdatademo.bean.Product;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

/**
 *Dao操作接口
 * 泛型：Product 是 操作的实体类。
 * Long 是主键
 */
@Repository
public interface ProductDao extends ElasticsearchRepository<Product,Long>{
}
